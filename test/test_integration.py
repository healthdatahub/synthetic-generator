"""
A tester:
- la génération pour plusieurs composantes connectées (vérification de la création de fichiers non vides)
- le randomgenerator sur quelques cas
- la génération par le volume
"""

import pytest
from .conf import process_test
from ..utils.utils import generate_links_table,generate_graph_from_links
import pandas as pd
import networkx as nx
from ..metagraphGenerator import MetaGraphGenerator

process = process_test
