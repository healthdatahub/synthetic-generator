import fnmatch
import re

def glob2regex(string):
    return fnmatch.translate(string)

class SchemaModifier():

    def __init__(self,modif:str):
        """
        Encapsulates a modification to be made to the schemas when built.
        Parameters
        ----------
        modif: str, expected to be like table:variable:property:newvalue
        """
        modif = modif.split('|')
        self.table = glob2regex(modif[0])
        self.variable = glob2regex(modif[1])
        self.property = modif[2]
        self.new_value = modif[3]

    def run(self,var_schemas):
        """
        Apply the defined modification to the var schema
        """
        for table, schema in var_schemas.items():
            if re.match(self.table,table):
                for var,props in schema.items():
                    if re.match(self.variable,var):
                        for property in props:
                            if re.match(self.property,property):
                                var_schemas[table][var][property] = self.new_value
                                #if the format is changed, remove the nomenclature
                                if property=='format':
                                    var_schemas[table][var]['nomenclature'] = '-'
