import networkx as nx
import pandas as pd

def find_shortest_path(table1,
                        table2,
                        graph,
                        links,
                        return_lists = False):
    """
    Finds the shortest path in a graph between two tables. 
    For instance: 
    given the simplistic graph A--B--C, 
    where A is linked to B through k1,
    and B is linked to C through k2 in B which is named k3 in C,
    and if you call `find_shortest_path(A,C,g,l,return_lists=False)`,
    the method will return 'A --> B : k1 => k1 \n B --> C : k2 => k3'
    and if you call `find_shortest_path(A,C,g,l,return_lists=True)`,
    the method will return [A,B,C], [k1,k2],[k1,k3]
    
    Parameters
    ----------
    table1: str, name of the start table in the graph
    table2: str, name of the end table in the graph
    graph: networkx.Graph object
    links: pandas.DataFrame, stores the list of primary and foreign keys between tables of the graph
    return_lists: bool, if True the method returns lists instead of a string
    
    Returns 
    -------
    if return_lists is set to False:
        path_sentence: str, human readable description of the path (which tables using which keys)
    otherwise:
        path: list, list of tables on the shortest path
        left_on_list: list, list of keys for each 'left table' (A  and B in the example)
        right_on_list: list, list of keys for each 'right table' (B and C in the example)
    """
    if isinstance(graph,str):
        G = nx.read_gpickle(graph)
    else: 
        G = graph
    if isinstance(links,str):
        links = pd.read_csv(links)
        
    path  = nx.shortest_path(G,table1,table2)
    path_sentence = ''
    right_on_list = []
    left_on_list = []
    for i in range(len(path)-1):
        row = links[(links['source']==path[i])&(links['target']==path[i+1])]
        left_on = row['left_on'].values
        right_on = row['right_on'].values
        left_on_list.append(left_on)
        right_on_list.append(right_on)
        path_sentence += '{} --> {} : {} => {}\n'.format(path[i],
                                                        path[i+1],
                                                        left_on,
                                                        right_on)
    if return_lists:
        return path, left_on_list,right_on_list

    return path_sentence[:-1]
