import pandas as pd
import numpy as np
from sklearn.metrics import pairwise_distances
import datetime as dt
from ..utils.shortest_path import find_shortest_path
import datetime

class DateCorrector():

    def __init__(self, table, var_schemas,ym_match,df_dates,df_matchings):
        self.table = table
        self.var_schemas = var_schemas
        self.ym_match = ym_match
        self.df_dates = df_dates
        self.df_matchings = df_matchings

    def correction_age(self,df):
        """
        Met en cohérence les âges avec les dates de naissance (et la date éventuelle de décès) dans les tables de cartographie
        L'âge n'est pas le vrai âge (à +- 1 an)
        """
        if 'age' in df.columns:
            if 'ben_nai_ann' in df.columns:
                fmt = self.var_schemas[self.table]['ben_dcd_dte']['format']
                if fmt=='default': fmt = "%d%b%Y:00:00:00"
                for i in range(len(df)):
                    end_year = min(dt.date.today().year,
                                    pd.to_datetime(df.iloc[i]['ben_dcd_dte'],format=fmt).year)
                    df.iloc[i]['age'] = int(df.iloc[i]['ben_nai_ann']) - end_year
        return df

    def correction_age_jou(self,df):
        """
        corrige age_jou: met à trois blancs quand AGE_ANN n'est pas 0
        """
        def withdraw_age_jou(age_ann):
            if age_ann != 0:
                return '   '
        if ('AGE_JOU' in df.columns) and ('AGE_ANN' in df.columns):
                df['AGE_JOU'] = df['AGE_ANN'].apply(withdraw_age_jou)
        return df

    def coherence_ym(self,df):
        """
        Assure la cohérence entre les dates au format mois+année avec les dates au format jour+mois+année
        arguments
        ---------
        df: pd.DataFrame à corriger
        name: nom de la table
        ym_match: table de matching entre les dates au format mois+année et les dates au format jour+mois+année
        """
        self.ym_match = self.ym_match[self.ym_match['table']==self.table]
        for var,match,match_fmt in zip(self.ym_match['var'],self.ym_match['match'],self.ym_match['match_format']):
            var_fmt = self.var_schemas[self.table][var]['format']
            if (var in df.columns) and (match in df.columns):
                for i in range(len(df)):
                    df.iloc[i][var] = pd.to_datetime(df.iloc[i][match],format=match_fmt).strftime(var_fmt)

        return df

    def correction_flx_trt(self,df):
        """
        Corrige les relations entre les variables FLX_TRT_DTD et FLX_TRT_DTF dans une table
        """
        if ('FLX_TRT_DTD' in df.columns) and ('FLX_TRT_DTF' in df.columns):
            #on peut rajouter des stats ici
            durations = np.random.randint(1,50,len(df))
            flx_fmt = self.var_schemas[self.table]['FLX_TRT_DTD']['format']
            if flx_fmt == 'default': flx_fmt = '%d%b%Y:%H:%M:%S'
            df['FLX_TRT_DTF'] = pd.to_datetime(df['FLX_TRT_DTD'],format=flx_fmt) + pd.to_timedelta(durations,unit='d')
            df['FLX_TRT_DTF'] = df['FLX_TRT_DTF'].dt.strftime(flx_fmt)
            if flx_fmt == '%d%b%Y:%H:%M:%S':
                df['FLX_TRT_DTF'] = df['FLX_TRT_DTF'].str.upper()
        return df


    def correction_start_end(self,df):
        """
        Corrige les ordres entre les dates de début et dates de fin
        arguments
        ---------
        df: pd.DataFrame, table à corriger
        name: str, nom de la table
        df_matchings: pd.DataFrame, contient les correspondances entre les dates de début et de fin
        """
        #check if there are any modifications
        matchings = self.df_matchings[self.df_matchings['table']==self.table]
        if matchings.shape[0]>0:
            for var,match in zip(matchings['var'],matchings['match']):
                var_fmt = self.var_schemas[self.table][var]['format']
                match_fmt = self.var_schemas[self.table][match]['format']
                if (var in df.columns) and (match in df.columns):
                    #possible d'injecter des stats ici
                    durations = np.random.randint(1,50,len(df))
                    df[match] = pd.to_datetime(df[var],format=var_fmt) + pd.to_timedelta(durations,unit='d')
                    #go back to correct format
                    df[match] = df[match].dt.strftime(match_fmt)
                    if match_fmt == '%d%b%Y:%H:%M:%S':
                        df[match] = df[match].str.upper()
        return df

    def correction_flux(self,df):
        """
        Assure EXE_SOI_DTF<FLX_TRT_DTD
        """
        if 'EXE_SOI_DTF' in df.columns and 'FLX_TRT_DTD' in df.columns:
            #possible d'injecter des stats mais inutile a priori
            durations = np.random.randint(1,50,len(df))
            #store formats
            exe_fmt = self.var_schemas[self.table]['EXE_SOI_DTF']['format']
            flx_fmt = self.var_schemas[self.table]['FLX_TRT_DTD']['format']
            #correct flux date
            df['FLX_TRT_DTD'] = pd.to_datetime(df['EXE_SOI_DTF'],format=exe_fmt) + pd.to_timedelta(durations,unit='d')
            #correct flx format
            df['FLX_TRT_DTD'] = df['FLX_TRT_DTD'].dt.strftime(flx_fmt)
            if flx_fmt == '%d%b%Y:%H:%M:%S':
                df['FLX_TRT_DTD'] = df['FLX_TRT_DTD'].str.upper()

        return df


    def make_all_dates_corrections(self,df):
        """
        Execute toutes les fonctions de correction dans le bon ordre
        df: table à corriger
        name: nom de la table
        ym_match: table de correspondance yearmonth
        start_end_match: table de correspondance start end
        """

        df = self.correction_start_end(df)
        df = self.correction_flux(df)
        df = self.correction_flx_trt(df)
        df = self.coherence_ym(df)
        df = self.correction_age(df)
        df = self.correction_age_jou(df)

        return df

def correct_dob_dod(df_store,date_vars,var_dict,tree,links):
    """
    correction de la date de naissance et de la date de décès
    """
    df_dates = {}
    for name,table in df_store.items():
        date_columns = date_vars[date_vars['table']==name]['var'].values
        date_formats = date_vars[date_vars['table']==name]['format'].values
        useful_columns = list(date_columns) + var_dict[name]
        df_dates[name] = table[list(set(useful_columns))]
        for date_col,date_fmt in zip(date_columns,date_formats):
            if date_fmt=='%d%b%Y:%H:%M:%S': #default format
                 df_dates[name][date_col] = df_dates[name][date_col].apply(lambda x:x.title())
            df_dates[name][date_col] = pd.to_datetime(df_dates[name][date_col],format = date_fmt)

    leaves = [node for node in tree.nodes if tree.out_degree(node)==0]
    leaves_w_dates = list(set(leaves).intersection(set(date_vars['table'].unique())))
    dates_id = {id:{'max':[],'min':[]} for id in df_dates['IR_BEN_R']['BEN_NIR_PSA'].unique()}
    for table in leaves_w_dates:
        try:
            path,left_on_list,right_on_list = find_shortest_path('IR_BEN_R',
                                                                 table,
                                                                 tree,
                                                                 links,
                                                                 return_lists=True)
        except:
            continue

        #merge along the path
        merged_branch = df_dates['IR_BEN_R']
        for int_table, left_on, right_on in zip(path[1:],left_on_list,right_on_list):
            merged_branch = merged_branch.merge(df_dates[int_table],
                                                left_on=left_on[0].split('+'),
                                                right_on=right_on[0].split('+')
                                               )
        #find maximum and minimum dates in merged_branch
        date_columns_in_branch = list(set(merged_branch.columns).intersection(set(date_vars['var'].unique())))
        min_date = merged_branch.groupby('BEN_NIR_PSA')[date_columns_in_branch].apply(lambda x:x.values.min()).reset_index()
        max_date = merged_branch.groupby('BEN_NIR_PSA')[date_columns_in_branch].apply(lambda x:x.values.max()).reset_index()
        for i in range(len(min_date)):
            nirano = min_date.iloc[i]['BEN_NIR_PSA']
            max_d = max_date.iloc[i].values[1]
            min_d = min_date.iloc[i].values[1]
            dates_id[nirano]['max'].append(max_d)
            dates_id[nirano]['min'].append(min_d)
    for patient,dates in dates_id.items():
        dates_id[patient]['min'] = np.min(dates['min'])
        dates_id[patient]['max'] = np.max(dates['max'])


    #compute dod and dob
    default_dod = '01JAN1600:00:00:00'
    dob_dod = {}
    for patient, dates in dates_id.items():
        dob_dod[patient] = {}
        dob_dod[patient]['dob'] = (dates['min'] - pd.to_timedelta(np.random.randint(1,3650,1),unit='d'))[0]
        dob_dod[patient]['dod'] = (dates['max'] + pd.to_timedelta(np.random.randint(1,3650,1),unit='d'))[0]
        if dob_dod[patient]['dod']>datetime.datetime.now():
            dob_dod[patient]['dod'] = default_dod
    id2dob = {i:d['dob'] for i,d in dob_dod.items()}
    id2dod = {i:d['dod'] for i,d in dob_dod.items()}

    #correction ben_nai_ann et BEN_NAI_MOI
    for table in df_store:
        if 'BEN_NAI_ANN' in df_store[table].columns and 'BEN_NIR_PSA' in df_store[table].columns:
            df_store[table]['BEN_NAI_ANN'] = pd.to_datetime(df_store[table]['BEN_NIR_PSA'].map(id2dob)).dt.year
        if 'BEN_NAI_MOI' in df_store[table].columns and 'BEN_NIR_PSA' in df_store[table].columns:
            df_store[table]['BEN_NAI_MOI'] = pd.to_datetime(df_store[table]['BEN_NIR_PSA'].map(id2dob)).dt.month

    # ce mapping devrait en toute rigueur être de id_carto vers (BEN_NIR_PSA,BEN_RNG_GEM)
    idcarto2psa = {idcarto:psa for idcarto,psa in zip(df_store['CT_IDE_AAAA_GN']['id_carto'],df_store['CT_IDE_AAAA_GN']['ben_nir_psa'])}

    for table in df_store:
        if 'ben_nai_ann' in df_store[table].columns and 'id_carto' in df_store[table].columns:
            df_store[table]['ben_nai_ann'] = pd.to_datetime(df_store[table]['id_carto'].map(idcarto2psa).map(id2dob)).dt.year


    #en toute rigueur, ce mapping devrait être de ben_idt_ano vers (ben_nir_psa,ben_rng_gem)
    idtano2psa = {idtano:psa for idtano,psa in zip(df_store['IR_BEN_R']['BEN_IDT_ANO'],df_store['IR_BEN_R']['BEN_NIR_PSA'])}

    for table in df_store:
        if 'BEN_NAI_ANN' in df_store[table].columns and 'BEN_IDT_ANO' in df_store[table].columns:
            df_store[table]['BEN_NAI_ANN'] =  pd.to_datetime(df_store[table]['BEN_IDT_ANO'].map(idtano2psa).map(id2dob)).dt.year
        if 'BEN_NAI_MOI' in df_store[table].columns and 'BEN_IDT_ANO' in df_store[table].columns:
            df_store[table]['BEN_NAI_MOI'] =  pd.to_datetime(df_store[table]['BEN_IDT_ANO'].map(idtano2psa).map(id2dob)).dt.month

    def find_ame_dod(x):
        y = id2dod[x]
        if y == default_dod:
            return '160001'
        y = str(pd.to_datetime(y).year)+str(pd.to_datetime(y).month)
        return y

    def find_dte_dod(x):
        y = id2dod[x]
        if y == default_dod:
            return y
        y = pd.to_datetime(y).strftime('%d%b%Y:00:00:00').upper()
        return y

    #correction pour IR_BEN_R
    df_store['IR_BEN_R']['BEN_DCD_AME'] = df_store['IR_BEN_R']['BEN_NIR_PSA'].apply(find_ame_dod)
    df_store['IR_BEN_R']['BEN_DCD_DTE'] = df_store['IR_BEN_R']['BEN_NIR_PSA'].apply(find_dte_dod)
    #correction pour ER_PRS_F
    df_store['ER_PRS_F']['BEN_DCD_AME'] = df_store['ER_PRS_F']['BEN_NIR_PSA'].apply(find_ame_dod)
    df_store['ER_PRS_F']['BEN_DCD_DTE'] = df_store['ER_PRS_F']['BEN_NIR_PSA'].apply(find_dte_dod)
    #correction pour la carto
    df_store['CT_DEP_AAAA_GN']['BEN_DCD_AME'] = df_store['CT_DEP_AAAA_GN']['id_carto'].map(idcarto2psa).apply(find_ame_dod)
    df_store['CT_DEP_AAAA_GN']['ben_dcd_dte'] = df_store['CT_DEP_AAAA_GN']['id_carto'].map(idcarto2psa).apply(find_dte_dod)
    df_store['CT_IND_AAAA_GN']['BEN_DCD_AME'] = df_store['CT_IND_AAAA_GN']['id_carto'].map(idcarto2psa).apply(find_ame_dod)
    df_store['CT_IND_AAAA_GN']['ben_dcd_dte'] = df_store['CT_IND_AAAA_GN']['id_carto'].map(idcarto2psa).apply(find_dte_dod)
    #correction pour le CépiDC
    df_store['KI_CCI_R']['BEN_DCD_AME'] = df_store['KI_CCI_R']['BEN_IDT_ANO'].map(idtano2psa).apply(find_ame_dod)
    df_store['KI_CCI_R']['BEN_DCD_DTE'] = df_store['KI_CCI_R']['BEN_IDT_ANO'].map(idtano2psa).apply(find_dte_dod)
    df_store['KI_ECD_R']['BEN_DCD_AME'] = df_store['KI_ECD_R']['BEN_IDT_ANO'].map(idtano2psa).apply(find_ame_dod)
    df_store['KI_ECD_R']['BEN_DCD_DTE'] = df_store['KI_ECD_R']['BEN_IDT_ANO'].map(idtano2psa).apply(find_dte_dod)
