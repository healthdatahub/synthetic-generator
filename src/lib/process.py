import os
import pandas as pd
import networkx as nx
from .config import Config
from .metagraphGenerator import MetaGraphGenerator
from .utils.utils import generate_links_table,generate_graph_from_links

class Process(Config):
    """
    Encapsulates the creation of the meta graph generator and its execution
    """

    def __init__(self,path):
        super().__init__(path)

    def build_resources(self):
        """
        Build the links table and the graph which represent the database.
        If they already exist, they are not computed anew.
        """

        if 'links.csv' in os.listdir(self.path2resources):
            self.links = pd.read_csv(os.path.join(self.path2resources,'links.csv'),keep_default_na=False)
        else:
            self.links = generate_links_table(self.path2schemas)
        if 'graph.pkl' in os.listdir(self.path2resources):
            self.graph = nx.read_gpickle(os.path.join(self.path2resources,'graph.pkl'))
        else:
            self.graph = generate_graph_from_links(self.links)

    def build_metagenerator(self):
        """
        Build the metagenerator from the links table and the graph of the database.
        Then builds all the elements useful for generating.
        """
        self.metagenerator = MetaGraphGenerator(
                                                self.graph,
                                                self.links,
                                                self.base_name,
                                                self.roots,
                                                self.separator
                                                )
        self.metagenerator.build_var_schemas(self.path2schemas,
                                             self.schema_modifiers)
        self.metagenerator.build_nomenclatures(self.path2nomenc)
        self.metagenerator.build_dates_matching(self.path2schemas)
        self.metagenerator.build_table_path(self.path2schemas)


    def run(self):
        """
        Actually generate data.
        """
        self.metagenerator.sample(self.n_beneficiaires,
                                  self.path2export)
